# OVERVIEW #

A U C T I O M O N G E R is the auction tracking app for the discerning WoW player. It lives on your desktop. It talks directly to bnet. It offers desktop alerts, adjustable polling, watches, filtering, and an integrated UI that works with your computer, not against it.

The name is a tribute to Dan Barrett's insane invention, B L A Z E M O N G E R, but offers no-where near the featureset of his amazing game.