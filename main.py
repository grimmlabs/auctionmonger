#!/usr/bin python
# File: main
#
# Author: Jeff Grimmett
#
# Date: 8/10/2014
#
# Functions supporting battle.net REST access
#
# (c) 2014 Grimmlabs, a fully owned subsidary of Industrial Strength Software
 #############################################################################

# System imports
import datetime
import logging
import sys

# local imports
from lib import bnetFuncs
from lib import config
from lib import log

def main():
    cfg = config.ConfigObject()
    log.initLogging(cfg)

    logging.critical("> A H M O N G E R %s >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" % cfg.version)
    start = datetime.datetime.now()

    # Get list of regions from bnet
    realms = bnetFuncs.getRealmList(cfg)

    logging.critical("> Done. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

if __name__ == "__main__":
    main()
