# utilFuncs.py
#
# Functions supporting various utility features
#
# (c) 2014 Verifone Systems, inc.
#
# Author: Jeff Grimmett (jeffreyg3@verifone.com)
 ############################################################################

import logging
import sys

def initLogging(cfg):
    """
    Initialize logging
    """
    myLog = logging.getLogger()

    form = logging.Formatter(fmt=cfg.logFormat, datefmt=cfg.logDateFormat)

    fh = logging.FileHandler(cfg.logName, 'w')
    fh.setFormatter(form)

    sh = logging.StreamHandler(stream=sys.stdout)
    sh.setFormatter(form)

    myLog.addHandler(fh)
    myLog.addHandler(sh)
    myLog.setLevel(cfg.logLevel)

    if not cfg.requestLogging:
        # Set request lib logging to CRIT
        requests_log = logging.getLogger("requests.packages.urllib3")
        requests_log.setLevel(logging.CRITICAL)
