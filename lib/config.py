#!/usr/bin python
#
# File: config
#
# Author: Jeff Grimmett
#
# Date: 8/10/2014
#
# Configuration object
#
# (c) 2014 Grimmlabs, a fully owned subsidary of Industrial Strength Software
 #############################################################################


import logging

class ConfigObject(object):
    """
    Object that holds all config variables.
    """
    version         = '1.0.0'

    # logging
    logName         = 'AHMONGER.log'
    logFormat       = '%(asctime)s | %(module)s.%(funcName)s.%(lineno)d %(levelname)s | %(message)s'
    logDateFormat   = '%Y-%m-%d %H:%M:%S'
    logLevel        = logging.DEBUG
    #logLevel        = logging.INFO
    #logLevel        = logging.CRITICAL

    # bnet
    bnetURL         = "http://us.battle.net"
    #crowdAppLogin   = ("arty-file-cleanup", "arty-cleanup") # username/pw for crowd api access
    #crowdMaxResults = 3000

    # requests lib logging
    requestLogging  = False # If set to True, you will see debug logging from requests lib, otherwise
                            # only critical and above

    #debug
    limit           = 0 # Limits how many users are in the report, for debug purposes. 0 = no limit.

