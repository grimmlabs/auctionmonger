#!/usr/bin python
# File: bnetFuncs
#
# Author: Jeff Grimmett
#
# Date: 8/10/2014
#
# Functions supporting battle.net REST access
#
# (c) 2014 Grimmlabs, a fully owned subsidary of Industrial Strength Software
 #############################################################################

import logging

import requests

def getRealmList(cfg):
    rc = []

    logging.debug("Getting realm list")
    r = requests.get("%s/api/wow/realm/status" % (cfg.bnetURL))

    if r.status_code == 200:
        results = r.json()
        rc = results['users']

    logging.info("Returning [%d] realms." % len(rc))
    return rc


def getUserFromCrowd(cfg, record):
    """
    Given a user link, retrieve that user's full record from Crowd, and return it.
    """

    logging.debug("Accessing user [%s] in Crowd." % record['name'])

    r = requests.get(record['link']['href'], auth=cfg.crowdAppLogin, headers = {'accept': 'application/json'})

    if r.status_code == 200:
        userRecord = r.json()
        return userRecord
    else:
        return {}

